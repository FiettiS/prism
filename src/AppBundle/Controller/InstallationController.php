<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class InstallationController extends Controller
{
    /**
     * @Route("/installation_get_all", name="installation_get_all")
     */
    public function getAllAction()
    {
        return new Response(json_encode($this->getDoctrine()->getManager()->getRepository('AppBundle:Installation')->getAllInstallations()), 200);
    }
}