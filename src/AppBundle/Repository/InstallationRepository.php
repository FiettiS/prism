<?php

namespace AppBundle\Repository;

/**
 * InstallationRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class InstallationRepository  extends \Doctrine\ORM\EntityRepository
{
    public function getAllInstallations()
    {
        return $this->createQueryBuilder('i')->getQuery()->getResult(\PDO::FETCH_ASSOC);
    }
}