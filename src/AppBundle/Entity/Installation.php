<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Installation
 *
 * @ORM\Entity(repositoryClass="AppBundle\Repository\InstallationRepository")
 * @ORM\Table(name="installation")
 */
class Installation
{
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, unique=true)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="name_displayed", type="string", length=255, unique=true)
     */
    private $displayedName;

    /**
     * @var string
     *
     * @ORM\Column(name="apikey", type="string", length=255)
     */
    private $apikey;

    /**
     * @var string
     *
     * @ORM\Column(name="longitude", type="decimal", precision=10, scale=6 )
     */
    private $longitude;

    /**
     * @var string
     *
     * @ORM\Column(name="latitude", type="decimal", precision=10, scale=6 )
     */
    private $latitude;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getDisplayedName(): string
    {
        return $this->displayedName;
    }

    /**
     * @return string
     */
    public function getApikey(): string
    {
        return $this->apikey;
    }

    /**
     * @return string
     */
    public function getLongitude(): string
    {
        return $this->longitude;
    }

    /**
     * @return string
     */
    public function getLatitude(): string
    {
        return $this->latitude;
    }

    /**
     * @param string $name
     * @return Installation
     */
    public function setName(string $name): Installation
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @param string $displayedName
     * @return Installation
     */
    public function setDisplayedName(string $displayedName): Installation
    {
        $this->displayedName = $displayedName;
        return $this;
    }

    /**
     * @param string $apikey
     * @return Installation
     */
    public function setApikey(string $apikey): Installation
    {
        $this->apikey = $apikey;
        return $this;
    }

    /**
     * @param string $longitude
     * @return Installation
     */
    public function setLongitude(string $longitude): Installation
    {
        $this->longitude = $longitude;
        return $this;
    }

    /**
     * @param string $latitude
     * @return Installation
     */
    public function setLatitude(string $latitude): Installation
    {
        $this->latitude = $latitude;
        return $this;
    }

}